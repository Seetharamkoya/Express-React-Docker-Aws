import os
import gitlab

# Connect to the GitLab instance using the GitLab API.
gl = gitlab.Gitlab('https://gitlab.com/', private_token='glpat-3rWnWKs8SYt9QWicEPw8')


# Fetch the status of jobs in a specified GitLab project.
def monitor_jobs(project_id):
    project = gl.projects.get(project_id)
    pipelines = project.pipelines.list()
    jobs = project.jobs.list()
    for job in jobs:
        print(f"Job: {job.name} | Status: {job.status}")
    monitor_jobs(project_id)


#Fetch the job logs for the specified GitLab project, pipeline, and job.
def fetch_job_logs(project_id, job_id):
    project = gl.projects.get(project_id)
    job = project.jobs.get(job_id)
    return job.trace()

